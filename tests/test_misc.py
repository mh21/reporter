from unittest import TestCase

import responses

from reporter.data import CheckoutData

BUILDS = [
        {
            'id': 0,
            'misc': {'iid': 0},
        },
        {
            'id': 1,
            'misc': {'iid': 1},
        },
    ]

CHECKOUT = {'id': 'redhat:123',
            'misc': {'iid': 0}}
ISSUES = []
TESTS = []

KCIDBFILE = {'builds': BUILDS,
             'checkouts': [CHECKOUT],
             'issueoccurrences': ISSUES,
             'testresults': [],
             'tests': TESTS}


class TestRequests(TestCase):
    """Tests for the API requests done by reporter."""

    @responses.activate
    def test_build_list(self):
        """Test the listing of builds."""
        responses.get(
            'http://localhost/api/1/kcidb/checkouts/0/all',
            json=KCIDBFILE
        )
        responses.get(
            'http://localhost/api/1/kcidb/checkouts/0',
            json=CHECKOUT
        )

        checkout = CheckoutData(0)
        builds = checkout.build_data.builds
        self.assertEqual(2, len(builds))
        self.assertEqual(
            [BUILDS[0]['id'], BUILDS[1]['id']],
            [build.id for build in builds]
        )

        # Make sure the builds iterator is reusable
        self.assertEqual(
            [BUILDS[0]['id'], BUILDS[1]['id']],
            [build.id for build in builds]
        )
